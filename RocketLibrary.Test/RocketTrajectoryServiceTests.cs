﻿using Moq;
using NUnit.Framework;
using RocketLibrary.Application.Dtos;
using RocketLibrary.Application.Services;
using RocketLibrary.Application.Validators;
using RocketLibrary.Application.Validators.Models;
using RocketLibrary.Enums;
using RocketLibrary.Persistent.Repositories;
using System;
using System.Collections.Generic;

namespace RocketLibrary.Test
{
    [TestFixture]
    class RocketTrajectoryServiceTests
    {
        List<ITrajectoryValidator> _trajectoryValidators;
        Mock<ITrajectoryValidator> _trajectory1Validator;
        Mock<IRocketRepository> _rocketRepository;
        RocketDto _rocketDto;

        [SetUp]
        public void SetUp() 
        {
            _rocketRepository = new Mock<IRocketRepository>();
            _trajectoryValidators = new List<ITrajectoryValidator>();

            _rocketDto = new RocketDto
            {
                RocketId = Guid.NewGuid(),
                X = 5,
                Y = 5
            };

            _trajectory1Validator = new Mock<ITrajectoryValidator>();
            _trajectory1Validator.Setup(x => x.Validate(_rocketDto))
                .Returns(new ValidationModel(_rocketDto.RocketId, TrajectoryStatus.OkForLanding));

            _trajectoryValidators.Add(_trajectory1Validator.Object);
        }

        [Test]
        public void CheckTrajectory_Returns_OkForLanding()
        {
            var trajectory2Validator = new Mock<ITrajectoryValidator>();
            trajectory2Validator.Setup(x => x.Validate(_rocketDto))
                .Returns(new ValidationModel(_rocketDto.RocketId, TrajectoryStatus.OkForLanding));

            _trajectoryValidators.Add(trajectory2Validator.Object);

            var rocketTrajectoryService = new RocketTrajectoryService(_trajectoryValidators, _rocketRepository.Object);
            var result = rocketTrajectoryService.CheckTrajectory(_rocketDto);

            Assert.AreEqual("Ok for landing", result);
        }

        [Test]
        public void CheckTrajectory_Returns_Clash()
        {
            var trajectory2Validator = new Mock<ITrajectoryValidator>();
            trajectory2Validator.Setup(x => x.Validate(_rocketDto))
                .Returns(new ValidationModel(_rocketDto.RocketId, TrajectoryStatus.Clash));

            _trajectoryValidators.Add(trajectory2Validator.Object);

            var rocketTrajectoryService = new RocketTrajectoryService(_trajectoryValidators, _rocketRepository.Object);
            var result = rocketTrajectoryService.CheckTrajectory(_rocketDto);

            Assert.AreEqual("Clash", result);
        }

        [Test]
        public void CheckTrajectory_Returns_OutOfPlatform()
        {
            var trajectory2Validator = new Mock<ITrajectoryValidator>();
            trajectory2Validator.Setup(x => x.Validate(_rocketDto))
                .Returns(new ValidationModel(_rocketDto.RocketId, TrajectoryStatus.OutOfPlatform));

            _trajectoryValidators.Add(trajectory2Validator.Object);

            var rocketTrajectoryService = new RocketTrajectoryService(_trajectoryValidators, _rocketRepository.Object);
            var result = rocketTrajectoryService.CheckTrajectory(_rocketDto);

            Assert.AreEqual("Out of platform", result);
        }
    }
}
