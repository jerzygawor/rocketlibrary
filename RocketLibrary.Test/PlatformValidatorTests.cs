﻿using Moq;
using NUnit.Framework;
using RocketLibrary.Application.Dtos;
using RocketLibrary.Enums;
using RocketLibrary.Domain.Entities;
using RocketLibrary.Persistent.Repositories;
using RocketLibrary.Application.Validators;
using System;

namespace RocketLibrary.Test
{
    [TestFixture]
    public class PlatformValidatorTests
    {
        private Mock<IPlatformRepository> _repository;

        [SetUp]
        public void Setup()
        {
            var platformEntity = new PlatformEntity(5, 5, 10, 10);

            _repository = new Mock<IPlatformRepository>();
            _repository.Setup(x => x.Get()).Returns(platformEntity);
        }

        [Test]
        [TestCase(5,6)]
        [TestCase(10,10)]
        [TestCase(9,5)]
        [TestCase(5,10)]
        public void CollisionValidator_OkForLanding(int x, int y)
        {
            var entity = new RocketDto()
            {
                RocketId = Guid.NewGuid(),
                X = x,
                Y = y
            };

            LandingValidator validator = new LandingValidator(_repository.Object);
            var result = validator.Validate(entity);

            Assert.AreEqual(entity.RocketId, result.RocketId);
            Assert.AreEqual(TrajectoryStatus.OkForLanding, result.TrajectoryStatus);
        }

        [Test]
        [TestCase(5, 4)]
        [TestCase(11, 10)]
        [TestCase(1, 51)]
        [TestCase(5, 11)]
        public void CollisionValidator_OutOfPlatform(int x, int y)
        {
            var entity = new RocketDto()
            {
                RocketId = Guid.NewGuid(),
                X = x,
                Y = y
            };

            LandingValidator validator = new LandingValidator(_repository.Object);
            var result = validator.Validate(entity);

            Assert.AreEqual(entity.RocketId, result.RocketId);
            Assert.AreEqual(TrajectoryStatus.OutOfPlatform, result.TrajectoryStatus);
        }
    }
}
