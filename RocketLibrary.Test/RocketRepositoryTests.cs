﻿using NUnit.Framework;
using RocketLibrary.Domain.Entities;
using RocketLibrary.Persistent.Cache;
using RocketLibrary.Persistent.Repositories;
using System;

namespace RocketLibrary.Test
{
    [TestFixture]
    public class RocketRepositoryTests
    {
        [Test]
        public void RocketRepository_CreateEntityInCacheWorks() 
        {
            var rocketEntity = new RocketEntity(Guid.NewGuid(), 5, 6);
            var rocketEntity2 = new RocketEntity(Guid.NewGuid(), 2, 3);

            var rocketCache = new RocketCache();
            var repository = new RocketRepository(rocketCache);

            repository.Create(rocketEntity);
            var result = repository.Get();

            Assert.AreEqual(rocketEntity.Id, result.Id);
            Assert.AreEqual(rocketEntity.X, result.X);
            Assert.AreEqual(rocketEntity.Y, result.Y);

            repository.Create(rocketEntity2);
            result = repository.Get();

            Assert.AreEqual(rocketEntity2.Id, result.Id);
            Assert.AreEqual(rocketEntity2.X, result.X);
            Assert.AreEqual(rocketEntity2.Y, result.Y);
        }
    }
}
