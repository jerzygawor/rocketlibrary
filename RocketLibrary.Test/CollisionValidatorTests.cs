using Moq;
using NUnit.Framework;
using RocketLibrary.Application.Dtos;
using RocketLibrary.Enums;
using RocketLibrary.Domain.Entities;
using RocketLibrary.Persistent.Repositories;
using RocketLibrary.Application.Validators;
using System;

namespace RocketLibrary.Test
{
    [TestFixture]
    public class CollisionValidatorTests
    {
        [Test]
        public void CollisionValidator_OkForLanding_NotExistsPreviousRocket()
        {
            var entity = new RocketDto
            {
                RocketId = Guid.NewGuid(),
                X = 5,
                Y = 6
            };

            var repository = new Mock<IRocketRepository>();
            repository.Setup(x => x.Get()).Returns<RocketEntity>(null);

            CollisionValidator validator = new CollisionValidator(repository.Object);
            var result = validator.Validate(entity);


            Assert.AreEqual(entity.RocketId, result.RocketId);
            Assert.AreEqual(TrajectoryStatus.OkForLanding, result.TrajectoryStatus);
        }

        [Test]
        public void CollisionValidator_OkForLanding_TheSameRocketAsPrevious()
        {
            var rocketId = Guid.NewGuid();
            var previousEntity = new RocketDto
            {
                RocketId = rocketId,
                X = 7,
                Y = 9
            };

            var nextEntity = new RocketDto
            {
                RocketId = rocketId,
                X = 5,
                Y = 6
            };

            var repository = new Mock<IRocketRepository>();
            repository.Setup(x => x.Get()).Returns(previousEntity.ToRocketEntity());

            CollisionValidator validator = new CollisionValidator(repository.Object);
            var result = validator.Validate(nextEntity);

            Assert.AreEqual(nextEntity.RocketId, result.RocketId);
            Assert.AreEqual(TrajectoryStatus.OkForLanding, result.TrajectoryStatus);
        }

        [Test]
        [TestCase(1,6,4,5)]
        [TestCase(1,5,4,5)]
        [TestCase(4,1,2,3)]
        [TestCase(2,1,2,3)]
        [TestCase(1,1,2,3)]
        public void CollisionValidator_OkForLanding_NoCollisionWithPreviousRocket(
            int nextX, int nextY, int preX, int preY)
        {
            var previousEntity = new RocketDto
            {
                RocketId = Guid.NewGuid(),
                X = preX,
                Y = preY
            };

            var nextEntity = new RocketDto
            {
                RocketId = Guid.NewGuid(),
                X = nextX,
                Y = nextY
            };

            var repository = new Mock<IRocketRepository>();
            repository.Setup(x => x.Get()).Returns(previousEntity.ToRocketEntity());

            CollisionValidator validator = new CollisionValidator(repository.Object);
            var result = validator.Validate(nextEntity);

            Assert.AreEqual(nextEntity.RocketId, result.RocketId);
            Assert.AreEqual(TrajectoryStatus.OkForLanding, result.TrajectoryStatus);
        }

        [Test]
        [TestCase(3, 6, 4, 5)]
        [TestCase(3, 5, 4, 5)]
        [TestCase(3, 2, 2, 3)]
        [TestCase(2, 3, 2, 3)]
        [TestCase(1, 4, 2, 3)]
        public void CollisionValidator_Clash_CollisionWithPreviousRocket(
            int nextX, int nextY, int preX, int preY)
        {
            var previousEntity = new RocketDto
            {
                RocketId = Guid.NewGuid(),
                X = preX,
                Y = preY
            };

            var nextEntity = new RocketDto
            {
                RocketId = Guid.NewGuid(),
                X = nextX,
                Y = nextY
            };

            var repository = new Mock<IRocketRepository>();
            repository.Setup(x => x.Get()).Returns(previousEntity.ToRocketEntity());

            CollisionValidator validator = new CollisionValidator(repository.Object);
            var result = validator.Validate(nextEntity);

            Assert.AreEqual(nextEntity.RocketId, result.RocketId);
            Assert.AreEqual(TrajectoryStatus.Clash, result.TrajectoryStatus);
        }
    }
}