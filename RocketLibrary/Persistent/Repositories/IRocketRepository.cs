﻿using RocketLibrary.Domain.Entities;

namespace RocketLibrary.Persistent.Repositories
{
    public interface IRocketRepository
    {
        RocketEntity Get();
        void Create(RocketEntity rocketModel);
    }
}
