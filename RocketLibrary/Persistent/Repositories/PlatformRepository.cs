﻿using RocketLibrary.Persistent.Cache;
using RocketLibrary.Domain.Entities;

namespace RocketLibrary.Persistent.Repositories
{
    public class PlatformRepository : IPlatformRepository
    {
        private readonly RocketCache _cache;

        public PlatformRepository(RocketCache cache)
        {
            _cache = cache;
        }

        public PlatformEntity Get()
        {
            return _cache.Get<PlatformEntity>("Platform");
        }

        public void Set(PlatformEntity platformEntity)
        {
            _cache.Set("Platform", platformEntity);
        }
    }
}
