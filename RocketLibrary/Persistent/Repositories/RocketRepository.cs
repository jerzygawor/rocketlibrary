﻿using RocketLibrary.Persistent.Cache;
using RocketLibrary.Domain.Entities;

namespace RocketLibrary.Persistent.Repositories
{
    public class RocketRepository : IRocketRepository
    {
        private readonly RocketCache _cache;
        public RocketRepository(RocketCache cache)
        {
            _cache = cache;
        }

        public void Create(RocketEntity rocketModel)
        {
            _cache.Set("Rocket", rocketModel);
        }

        public RocketEntity Get()
        {
            return _cache.Get<RocketEntity>("Rocket");
        }
    }
}
