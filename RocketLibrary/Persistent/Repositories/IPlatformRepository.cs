﻿using RocketLibrary.Domain.Entities;

namespace RocketLibrary.Persistent.Repositories
{
    public interface IPlatformRepository
    {
        PlatformEntity Get();
        void Set(PlatformEntity platformEntity);
    }
}
