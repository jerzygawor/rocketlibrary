﻿using Microsoft.Extensions.Caching.Memory;

namespace RocketLibrary.Persistent.Cache
{
    public class RocketCache
    {
        private MemoryCache _cache = new MemoryCache(new MemoryCacheOptions());

        public TItem Get<TItem>(object key)
        {
            TItem cacheEntry;
            if (!_cache.TryGetValue(key, out cacheEntry))
            {
                return default(TItem);
            }
            return cacheEntry;
        }

        public void CleareCache(object key)
        {
            _cache.Remove(key);
        }

        public void Set<TItem>(object key, TItem item)
        {
            _cache.Set(key, item);
        }
    }
}