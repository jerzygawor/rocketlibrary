﻿using System.ComponentModel;

namespace RocketLibrary.Enums
{
    public enum TrajectoryStatus
    {
        [Description("Out of platform")]
        OutOfPlatform,
        [Description("Clash")]
        Clash,
        [Description("Ok for landing")]
        OkForLanding
    }
}
