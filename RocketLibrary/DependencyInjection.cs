﻿using Microsoft.Extensions.DependencyInjection;
using RocketLibrary.Persistent.Cache;
using RocketLibrary.Application.Services;
using RocketLibrary.Application.Validators;

namespace RocketLibrary
{
    public static class DependencyInjection
    {
        public static void AddRocketLibrary(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<ITrajectoryValidator, LandingValidator>();
            serviceCollection.AddScoped<ITrajectoryValidator, CollisionValidator>();
            serviceCollection.AddScoped<IRocketTrajectoryService, RocketTrajectoryService>();

            serviceCollection.AddSingleton<RocketCache>();
        }
    }
}
