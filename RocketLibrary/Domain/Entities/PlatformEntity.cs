﻿namespace RocketLibrary.Domain.Entities
{
    public class PlatformEntity
    {
        public int StartX { get; private set; }
        public int StartY { get; private set; }
        public int EndX { get; private set; }
        public int EndY { get; private set; }

        public PlatformEntity(
            int startX,
            int startY,
            int endX,
            int endY)
        {
            StartX = startX;
            StartY = startY;
            EndX = endX;
            EndY = endY;
        }
    }
}
