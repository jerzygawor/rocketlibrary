﻿using System;

namespace RocketLibrary.Domain.Entities
{
    public class RocketEntity
    {
        public Guid Id { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        public RocketEntity(
            Guid id,
            int x,
            int y)
        {
            Id = id;
            X = x;
            Y = y;
        }
    }
}
