﻿using RocketLibrary.Domain.Entities;

namespace RocketLibrary.Application.Dtos
{
    public class PlatformDto
    {
        public int StartX { get; set; }
        public int StartY { get; set; }
        public int EndX { get; set; }
        public int EndY { get; set; }

        public PlatformEntity ToPlatformEntity()
        {
            return new PlatformEntity(StartX, StartY, EndX, EndY);
        }
    }
}
