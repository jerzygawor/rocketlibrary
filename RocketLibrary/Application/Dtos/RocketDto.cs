﻿using RocketLibrary.Domain.Entities;
using System;

namespace RocketLibrary.Application.Dtos
{
    public class RocketDto
    {
        public Guid RocketId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public RocketEntity ToRocketEntity()
        {
            return new RocketEntity(RocketId, X, Y);
        }
    }
}
