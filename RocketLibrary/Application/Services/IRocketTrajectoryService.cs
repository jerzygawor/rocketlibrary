﻿using RocketLibrary.Application.Dtos;

namespace RocketLibrary.Application.Services
{
    public interface IRocketTrajectoryService
    {
        string CheckTrajectory(RocketDto rocketModel);
    }
}
