﻿using RocketLibrary.Common.Extensions;
using RocketLibrary.Application.Dtos;
using RocketLibrary.Enums;
using RocketLibrary.Persistent.Repositories;
using RocketLibrary.Application.Validators;
using System.Collections.Generic;

namespace RocketLibrary.Application.Services
{
    public class RocketTrajectoryService : IRocketTrajectoryService
    {
        private readonly IEnumerable<ITrajectoryValidator> _validators;
        private readonly IRocketRepository _rocketRepository;

        public RocketTrajectoryService(
            IEnumerable<ITrajectoryValidator> validators,
            IRocketRepository rocketRepository)
        {
            _validators = validators;
            _rocketRepository = rocketRepository;
        }

        public string CheckTrajectory(RocketDto rocketDto)
        {
            foreach(var validator in _validators)
            {
                var result = validator.Validate(rocketDto);

                if (result.TrajectoryStatus != TrajectoryStatus.OkForLanding)
                    return result.TrajectoryStatus.ToDescriptionString();
            }

            _rocketRepository.Create(rocketDto.ToRocketEntity());

            return TrajectoryStatus.OkForLanding.ToDescriptionString();
        }
    }
} 