﻿using RocketLibrary.Application.Dtos;

namespace RocketLibrary.Application.Services
{
    public interface IPlatformConfigurationService
    {
        void SetPlatform(PlatformDto platformEntity);
    }
}
