﻿using RocketLibrary.Application.Dtos;
using RocketLibrary.Persistent.Repositories;

namespace RocketLibrary.Application.Services
{
    public class PlatformConfigurationService : IPlatformConfigurationService
    {
        private readonly IPlatformRepository _platformRepository;

        public PlatformConfigurationService(IPlatformRepository platformRepository)
        {
            _platformRepository = platformRepository;
        }

        public void SetPlatform(PlatformDto platformDto)
        {
            //Here will be helpful to add validation if StartX <= Endx and so on

            _platformRepository.Set(platformDto.ToPlatformEntity());
        }
    }
}
