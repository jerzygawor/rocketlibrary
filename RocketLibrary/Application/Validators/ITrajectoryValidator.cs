﻿using RocketLibrary.Application.Dtos;
using RocketLibrary.Application.Validators.Models;

namespace RocketLibrary.Application.Validators
{
    public interface ITrajectoryValidator
    {
        ValidationModel Validate(RocketDto rocketModel);
    }
}
