﻿using RocketLibrary.Application.Dtos;
using RocketLibrary.Enums;
using RocketLibrary.Persistent.Repositories;
using RocketLibrary.Application.Validators.Models;

namespace RocketLibrary.Application.Validators
{
    public class CollisionValidator : ITrajectoryValidator
    {
        private readonly IRocketRepository _rocketRepository;
        public CollisionValidator(IRocketRepository rocketRepository)
        {
            _rocketRepository = rocketRepository;
        }

        public ValidationModel Validate(RocketDto rocketModel)
        {
            var _previousLanding = _rocketRepository.Get();

            if(_previousLanding == null)
            {
                return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OkForLanding);
            }

            if(rocketModel.RocketId == _previousLanding.Id)
            {
                return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OkForLanding);
            }

            if(rocketModel.X < _previousLanding.X - 1 || rocketModel.X > _previousLanding.X + 1)
            {
                return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OkForLanding);
            }

            if (rocketModel.Y < _previousLanding.Y - 1 || rocketModel.Y > _previousLanding.Y + 1)
            {
                return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OkForLanding);
            }

            return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.Clash);
        }
    }
}
