﻿using RocketLibrary.Application.Dtos;
using RocketLibrary.Enums;
using RocketLibrary.Persistent.Repositories;
using RocketLibrary.Application.Validators.Models;

namespace RocketLibrary.Application.Validators
{
    public class LandingValidator : ITrajectoryValidator
    {
        private readonly IPlatformRepository _platformRepository;

        public LandingValidator(IPlatformRepository platformRepository)
        {
            _platformRepository = platformRepository;
        }

        public ValidationModel Validate(RocketDto rocketModel)
        {
            var platformEntity = _platformRepository.Get();

            if (rocketModel.X < platformEntity.StartX || rocketModel.X > platformEntity.EndX)
            {
                return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OutOfPlatform);
            }
            if (rocketModel.Y < platformEntity.StartY || rocketModel.Y > platformEntity.EndY)
            {
                return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OutOfPlatform);
            }

            return new ValidationModel(rocketModel.RocketId, TrajectoryStatus.OkForLanding);
        }
    }
}
