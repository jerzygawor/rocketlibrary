﻿using RocketLibrary.Enums;
using System;

namespace RocketLibrary.Application.Validators.Models
{
    public class ValidationModel
    {
        public Guid RocketId { get; private set; }
        public TrajectoryStatus TrajectoryStatus { get; private set; }

        public ValidationModel(Guid rocketId, TrajectoryStatus trajectoryStatus)
        {
            RocketId = rocketId;
            TrajectoryStatus = trajectoryStatus;
        }
    }
}
